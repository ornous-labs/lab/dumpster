export interface BrainDump {
    content: string;
    completed: boolean;
    createdAt: datetime;
    lastUpdatedAt: datetime;
}
